# Kubernetes CICD


# Lest use a Kubernetes playground at Katacoda
https://katacoda.com/courses/kubernetes/playground

## Deploy gitlab-runner into kubernetes
For this I'll use helm
https://helm.sh/docs/intro/install/

To install helm I'll use the easy and probably insecure way because I dont care about security :)
```
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh 
```
Prepare helm
```
# helm init
helm repo add gitlab https://charts.gitlab.io
```
Change the token key in you repository
```
# settings/CI/CD/Runners
# expand and copy the token

#now in your repo
#kubernetes/values, l4
runnerRegistrationToken: "YOUR TOKEN HERE"
```
Create the namespace gitlab
```
kubectl create namespace gitlab
kubectl get namespaces
```

Install runner with the configuration in your repo
```
git clone https://gitlab.com/josefloressv/kubernetes-cicd.git
cd kubernetes-cicd/kubernetes
helm install --namespace gitlab -f values.yaml gitlab-runner gitlab/gitlab-runner
```
Show the pods
```
kubectl get pods -n gitlab
```
Monitor pipeline in Kubernetes
```
kubectl get pods -n gitlab -w
```

Notes: Add new environment var to your repository
https://docs.gitlab.com/ee/ci/variables/
```
# settings/CI/CD/Variables
# expand and add new variable, clear [ ] Protect variable 

DEPLOYMENT_NAMESPACE:simpsons
```

for deploymen stage, check the new namespace and service
```
kubectl get pods -n simpsons
kubectl get services -n simpsons

# and you could see your deploy in port 30081
```
